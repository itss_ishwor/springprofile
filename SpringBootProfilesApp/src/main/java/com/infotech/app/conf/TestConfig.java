package com.infotech.app.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("test")
public class TestConfig
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Profile("dev")
    @Bean
    public String testDev(){
        System.out.println("test valueee is"+name);
        return  "";
    }
}
